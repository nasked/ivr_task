# IVR Tasks

This project is a middleware that simulates a connection between IVR and Stripe API.
## Getting Started

To get you started you migth need some specific frameworks previously installed in your machine.

### Prerequisites for Backend
```
Django (2.2)
DjangoRestFramework (3.10.0)
```

### Installing all requirements.

For installing all requirements just need to run the next file:
```
pip install -r requirements.txt
```


### Running the project
Now, you will need to run the next commands. Please note that you will need to setup your own database.

```
python manage.py migrate
python manage.py runserver

```

And that's it. You can access the api in the localhost:8000 (You can change the port if its needed)

### Testing the middleware
If the project is running, you can test it sending the next JSON:
```
{
    "trans_id": "321654351687",
    "exp_date": "1220",
    "cc_num": "4242424242424242",
    "cvv": "123"
}
```

### Looking the data
You can access the stored data in the database by creating a superuser:
```
python manage.py createsuperuser

```

then, you can login at localhost:8000/admin and see the corresponding information.

Additionally, you can check all the log information by accessing the folder /logs/.


## Documentation
If you want to check the documentation you must run the next command:
```

cd docs
sphinx-build -b html source build

```

then you can navigate in the browser with the next commands:
```

cd build
python -m http.server 9000

```

## Author
* Eduardo Navarro 

