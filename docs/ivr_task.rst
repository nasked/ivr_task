ivr\_task package
=================

Submodules
----------

ivr\_task.settings module
-------------------------

.. automodule:: ivr_task.settings
   :members:
   :undoc-members:
   :show-inheritance:

ivr\_task.urls module
---------------------

.. automodule:: ivr_task.urls
   :members:
   :undoc-members:
   :show-inheritance:

ivr\_task.wsgi module
---------------------

.. automodule:: ivr_task.wsgi
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: ivr_task
   :members:
   :undoc-members:
   :show-inheritance:
