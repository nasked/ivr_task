from django.db import models
from django.core.validators import RegexValidator

class RequestsCreditCard(models.Model):
    """
    Model in charge of validate and save the data coming in the request. Request only has and needs 4 fields.
    """

    trans_id = models.CharField(max_length=100)
    credit_card_number_validator = RegexValidator(regex=r"^\d*$")
    cc_num = models.CharField("Credit Card number", validators=[credit_card_number_validator],
                              max_length=16, null=False)
    cvv_validator = RegexValidator(regex=r"^\d*$")
    cvv = models.CharField("CVV", validators=[cvv_validator], max_length=4, null=False)
    exp_date_validator = RegexValidator(regex=r"^\d*$")
    exp_date = models.CharField("Expiration date",validators=[exp_date_validator], max_length=4, null=False)

class ResponseCreditCard(models.Model):
    """
    Model in charge of validate and save the data coming in the response from Stripe.
    """
    address_city = models.CharField(max_length=60, null=True, blank=True)
    address_country = models.CharField(max_length=60, null=True, blank=True)
    address_line1 = models.CharField(max_length=60, null=True, blank=True)
    address_line1_check = models.CharField(max_length=60, null=True, blank=True)
    address_line2 = models.CharField(max_length=60, null=True, blank=True)
    address_state = models.CharField(max_length=60, null=True, blank=True)
    address_zip = models.CharField(max_length=60, null=True, blank=True)
    address_zip_check = models.CharField(max_length=60, null=True, blank=True)
    brand = models.CharField(max_length=60, null=True, blank=True)
    country = models.CharField(max_length=60, null=True, blank=True)
    cvc_check = models.CharField(max_length=60, null=True, blank=True)
    dynamic_last4 = models.CharField(max_length=60, null=True, blank=True)
    exp_month = models.PositiveIntegerField()
    exp_year = models.PositiveIntegerField()
    fingerprint = models.CharField(max_length=60, null=True, blank=True)
    funding = models.CharField(max_length=60, null=True, blank=True)
    id_card = models.CharField(max_length=60, null=True, blank=True)
    last4 = models.CharField(max_length=60, null=True, blank=True)
    name = models.CharField(max_length=60, null=True, blank=True)
    object = models.CharField(max_length=60, null=True, blank=True)
    tokenization_method = models.CharField(max_length=60, null=True, blank=True)

class CreditCard(models.Model):
    """
    Model in charge of handle de credit card information, it has a FK to the Response Credit Card model.
    """
    client_ip = models.CharField(max_length=60, null=True, blank=True)
    created = models.CharField(max_length=60, null=True, blank=True)
    id_token = models.CharField(max_length=60, null=True, blank=True)
    livemode = models.BooleanField()
    object = models.CharField(max_length=60, null=True, blank=True)
    type = models.CharField(max_length=60, null=True, blank=True)
    used = models.BooleanField()
    rcc_id = models.ForeignKey(ResponseCreditCard, on_delete=models.CASCADE)

class Transaction(models.Model):
    """
    Model in charge of contains the request, it's for control.
    """
    request_id = models.ForeignKey(RequestsCreditCard, on_delete=models.CASCADE)
    response_id = models.ForeignKey(ResponseCreditCard, on_delete=models.CASCADE)
