from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from rest_framework import generics
from .models import RequestsCreditCard
from .serializers import RequestsCreditCardSerializer, ResponseCreditCardSerializer

class StripeRequest(generics.GenericAPIView):
    """
    This class handle the request after the middleware is done. You can access the data set in the request.
    """
    def post(self, request):
        id_token = request.credit_card.id_token
        fingerprint = request.response_credit_card.fingerprint
        trans_id = request.request_credit_card.trans_id

        return HttpResponse(f"Transaction id: {trans_id}, id token generated: {id_token} and the fingerprint: {fingerprint}")
