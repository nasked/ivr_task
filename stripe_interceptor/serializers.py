from stripe_interceptor.utils import mask_cvv, mask_card
from stripe_interceptor.models import RequestsCreditCard, ResponseCreditCard, CreditCard, Transaction
from rest_framework import serializers

class RequestsCreditCardSerializer(serializers.ModelSerializer):

    class Meta:
        model = RequestsCreditCard
        fields = '__all__'

    def save(self, data):
        """
        This method is in charge of save the credit card data with masks.
        :param data: Contains the payment information that must be masked.
        :return: It returns a credit_card object.
        """
        cvv = mask_cvv(data['cvv'])
        credit_card_number = mask_card(data['cc_num'])
        expiration_date = data['exp_date']
        transaction = data['trans_id']

        credit_card = RequestsCreditCard.objects.create(trans_id=transaction, cc_num=credit_card_number, cvv=cvv, exp_date=expiration_date)

        return credit_card


class ResponseCreditCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResponseCreditCard
        fields = "__all__"

class CreditCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = CreditCard
        fields = "__all__"

class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = "__all__"