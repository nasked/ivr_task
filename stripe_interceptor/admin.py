from django.contrib import admin
from .models import ResponseCreditCard, RequestsCreditCard, CreditCard, Transaction

# Register your models here.
admin.site.register(ResponseCreditCard)
admin.site.register(RequestsCreditCard)
admin.site.register(CreditCard)
admin.site.register(Transaction)
