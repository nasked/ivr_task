def mask_cvv(cvv):
    """
    This method just masks the cvv number with a lambda expression.
    :param cvv: Cvv number that must be masked
    :return: Cvv masked as *** or ****
    """
    cvv_masked = "".join(map(lambda x: "*", cvv))
    return cvv_masked

def mask_card(cc):
    """
    This method just masks the credit card number with a lambda expression.

    :param cc: Credit card number that must be masked
    :return: Credit card masked as ************4242
    """
    credit_card_masked = cc[-4:].rjust(len(cc), "*")
    return credit_card_masked


