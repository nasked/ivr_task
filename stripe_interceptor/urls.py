from django.urls import path
from .views import StripeRequest

urlpatterns = [
    path('token/', StripeRequest.as_view(), name="token"),
]