from django.apps import AppConfig


class StripeInterceptorConfig(AppConfig):
    name = 'stripe_interceptor'
