from .serializers import RequestsCreditCardSerializer, CreditCardSerializer, ResponseCreditCardSerializer, \
    TransactionSerializer
from stripe.error import AuthenticationError, RateLimitError, InvalidRequestError, CardError
from django.http import HttpResponse, JsonResponse
from rest_framework.status import HTTP_402_PAYMENT_REQUIRED, HTTP_400_BAD_REQUEST, HTTP_406_NOT_ACCEPTABLE, \
    HTTP_401_UNAUTHORIZED, HTTP_429_TOO_MANY_REQUESTS
from django.conf import settings
import re
import stripe
import json
import logging

logger = logging.getLogger(__name__)
stripe.api_key = settings.STRIPE_SECRET_KEY

NEXEMPT_URLS = [re.compile(settings.APPLY_URL.lstrip('/'))]


class StripeTransactionsMiddleware:
    """
    The StripeTransactionMiddleware class is a Middleware that will handle the connection between IVR apps and Stripe API
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """
        Call method is in charge of make all the logic before the view is called. This method will validate if the data
        matches with the models; and based on the result it will handle a response or a HTTP error response.

        This method only works with POST request and will only affect an specific view.

        :param request: Contains the payment information such as trans_id, cc_number, exp_date and cvv.
        :return: The method must return a request with more parameters that the beggining. Those params are added add
        the final if everything is correct otherwise it will return an HTTP error.
        """

        # Handling if the method must be applied or not to the view based on paths.
        path = request.path_info.lstrip('/')
        url_not_exempt = any(url.match(path) for url in NEXEMPT_URLS)

        if url_not_exempt:
            if request.method == "POST":

                try:
                    incoming_data = json.loads(request.body)
                    request_serializer = RequestsCreditCardSerializer(data=incoming_data)
                    if request_serializer.is_valid():
                        request_object = request_serializer.save(incoming_data)

                        stripe_response = stripe.Token.create(
                            card={
                                'number': request_serializer.validated_data['cc_num'],
                                # Extracting the first 2 values for the month
                                'exp_month': request_serializer.validated_data['exp_date'][:2],
                                # Extracting the last 2 values for the year
                                'exp_year': request_serializer.validated_data['exp_date'][2:],
                                'cvc': request_serializer.validated_data['cvv'],
                            },
                        )
                        logger.info(f'Stripe connection with Token endpoint made.')

                        response_serializer = ResponseCreditCardSerializer(data=stripe_response['card'])

                        if response_serializer.is_valid():
                            response_object = response_serializer.save()
                            logger.info(f'Response object saved.')

                            credit_card_data = {
                                "rcc_id": response_object.pk,
                                "client_ip": stripe_response["client_ip"],
                                "created": stripe_response["created"],
                                "id_token": stripe_response["id"],
                                "livemode": stripe_response["livemode"],
                                "object": stripe_response["object"],
                                "type": stripe_response["type"],
                                "used": stripe_response["used"],
                            }

                            logger.info(f'Credit card data generated')

                            creditcard_serializer = CreditCardSerializer(data=credit_card_data)

                            if creditcard_serializer.is_valid():
                                credit_card = creditcard_serializer.save()
                                logger.info(f'Credit card object saved')

                                transaction_data = {"request_id": request_object.pk, 'response_id': response_object.pk}
                                transaction_serializer = TransactionSerializer(data=transaction_data)

                                if transaction_serializer.is_valid():
                                    transaction_serializer.save()
                                    logger.info(f'Transaction object saved')

                                    setattr(request, "response_credit_card", response_object)
                                    setattr(request, "request_credit_card", request_object)
                                    setattr(request, "credit_card", credit_card)

                                    logger.info(f'Response object, credit card object and request object set to the request and sent to the view')
                                else:
                                    logging.error(
                                        f'Transaction information is not valid: {str(transaction_serializer.errors)}')
                                    return JsonResponse(
                                        {"error": request_serializer.errors,
                                         "description": "Information returned by Stripe did not match with serializer."},
                                        status=HTTP_406_NOT_ACCEPTABLE)
                            else:
                                logging.error(f'Credit information is not valid: {str(creditcard_serializer.errors)}')
                                return JsonResponse(
                                    {"error": request_serializer.errors,
                                     "description": "Information returned by Stripe did not match with serializer."},
                                    status=HTTP_406_NOT_ACCEPTABLE)
                        else:
                            logging.error(f'Response information is not valid: {str(response_serializer.errors)}')
                            return JsonResponse(
                                {"error": request_serializer.errors,
                                 "description": "Information returned by Stripe did not match with serializer."},
                                status=HTTP_406_NOT_ACCEPTABLE)
                    else:
                        logging.error(f'Credit card information is wrong: {str(request_serializer.errors)}')
                        return JsonResponse(
                            {"error": request_serializer.errors, "description": "Information provided is wrong."},
                            status=HTTP_400_BAD_REQUEST)
                except InvalidRequestError as error:
                    logger.error(f'Wrong parameters provided.: {str(error)}')
                    return JsonResponse(
                        {"error": str(error), "description": "Invalid parameters were provided to API"},
                        status=HTTP_400_BAD_REQUEST)
                except RateLimitError as error:
                    logger.error(f'Limit of requests exceeded: {str(error)}')
                    return JsonResponse(
                        {"error": str(error), "description": "Too many request to the api in short time"},
                        status=HTTP_429_TOO_MANY_REQUESTS)
                except CardError as error:
                    logger.error(f'The provided payment information is invalid.: {str(error)}')
                    return JsonResponse({"error": str(error), "description": "Payment information was invalid"},
                                        status=HTTP_400_BAD_REQUEST)
                except AuthenticationError as error:
                    logger.error(
                        f'Authentication failed: {str(error)}')
                    return JsonResponse(
                        {"errors": str(error),
                         "detail": "Authentication with API has failed",
                         },
                        status=HTTP_401_UNAUTHORIZED)

        response = self.get_response(request)

        return response
